# （Delphi）Android 权限动态申请控件

#### 介绍
![Gitee](https://gitee.com/sensor_wu/Delphi_Android_Permissions/raw/master/Image/Delphi_Android_Permissions.png)

在Delphi开发Android的过程中，有一种dangerous权限需要动态申请，就是说在程序运行的时候需要用户确认。此时需要开发者编写程序才可以实现，相对比较麻烦。所以就将其开发成一个控件，只需两行代码即可实现权限申请。
#### 适用范围
    Delphi 10.3
    Delphi 10.3.1
    Delphi 10.4
    Delphi 11
    Delphi 11.1

    Android 8.0
    Android 8.x
    Android 9.0
    Android 10.0
    Android 11.0

#### 目录结构
    Component Package Source
    Demo
    Icon
    image
    Screenshots


#### 通过控件包安装
    1. 用管理员权限打开Delphi，允许Delphi构建.BPL包到系统文件目录中
    2. 打开Component Package Source目录中的Android_Permissions.dpk文件
    3. 右键单击Android_Permissions.bpl文件选择Clean
    4. 右键单击Android_Permissions.bpl文件选择Install

    如果按右键没有出现Install菜单，请检查项目是否选择的是Windows 32-bit
  ![Gitee](https://gitee.com/sensor_wu/Delphi_Android_Permissions/raw/master/Screenshots/installed.png)  
    OK👌?
#### 控件属性
    Permissions: 是一个是一个包含所有dangerous权限的枚举列表TPermission = (ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, ACCESS_BACKGROUND_LOCATION, ACCESS_MEDIA_LOCATION,  ... )
#### 控件方法
    Apply: 申请权限方法，无参数，只要设置好Permissions属性，即可执行该方法，系统就会自动申请相关权限；
    IsPermissionGranted: 判断某个权限是否已经获取
#### 控件事件
    OnApplyResult: 系统授权结果事件，授权是否成功或者没有成功的原因
    procedure(const Sender : TObject; GrantedResult : Boolean; NoGranteds : TArray<string>) 
    如果授权申请成功， GrantedResult=true，NoGranteds无效，否则 GrantedResult=false，NoGranteds表示没有授权的项目列表        
#### 使用方法
##### 方式一
    1. 拖放TAndroid_Permission控件（在LW面板上）到Form上；
    2. 然后在通过控件属性选择需要的授权项目；
    3. 在程序中使用控件的Apply方法即可
##### 方式二
    var
    Android_Permission : TAndroid_Permission;
    try
        Android_Permission := TAndroid_Permission.Create(nil);
        Android_Permission.Permissions := Android_Permission.Permissions + [CAMERA];
        //....
        Android_Permission.Apply;
    finally
        Android_Permission.Free;
    end;    

#### Demo效果
![Gitee](https://gitee.com/sensor_wu/Delphi_Android_Permissions/raw/master/Screenshots/Result.png)  

#### Q&A
    1. 为什么使用了Apply后并没有获取到相应的权限？
    答：Android的权限分为三种：DANGEROUS、SIGNATURE、STANDARD。
    只有DANGEROUS权限才需要程序运行时请求用户确认，其它两种权限都是在开发环境中直接设置。
    但是对于DANGEROUS权限，也首先必须在Delphi的开发环境中勾选，然后才能通过程序申请，
    否则程序没有任何反应。
    2. 对于SIGNATURE、STANDARD这两种权限是否可以动态申请？
    答：不可以，对于Delphi来说，必须在开发环境中直接勾选。
    3. 这DANGEROUS、SIGNATURE、STANDARD三种权限的区别是什么？
    答：参见：https://blog.csdn.net/sensor_WU/article/details/125749637

#### 参与贡献
    sensor wu（老吴）
    
![Gitee](https://gitee.com/sensor_wu/Delphi_Android_Permissions/raw/master/Image/wechatme.png)  


#### 其它链接
    CSDN：http://t.csdn.cn/8rk1k
  
