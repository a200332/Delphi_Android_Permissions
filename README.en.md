# （Delphi）Android 权限动态申请控件

![Gitee](https://gitee.com/sensor_wu/Delphi_Android_Permissions/raw/master/Image/Delphi_Android_Permissions.png)
#### Description
In the process of Delphi developing Android, there is a kind of dangerous permission that needs to be dynamically applied, that is, the user needs to confirm when the program is running. At this time, developers need to write programs to achieve, which is relatively troublesome. So we develop it into a component, and only need two lines of code to realize permission application.

#### Software Categories
    Component Package
    Demo
    image
    screenshots

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
