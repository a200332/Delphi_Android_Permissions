{sensor wu 2022-07-14
}
unit uMainForm_Android_Permission;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,

  System.TypInfo,

  FMX.Controls.Presentation, FMX.StdCtrls, uAndroid_Permissions_Component,
  FMX.Memo.Types, FMX.ScrollBox, FMX.Memo, FMX.Layouts, FMX.Ani;

type
  TForm4 = class(TForm)
    Memo1: TMemo;
    Android_Permission: TAndroid_Permission;
    Layout1: TLayout;
    Layout2: TLayout;
    Layout3: TLayout;
    Layout4: TLayout;
    Layout5: TLayout;
    Label_Camera: TLabel;
    Switch_Camera: TSwitch;
    Layout6: TLayout;
    Label_ReadContacts: TLabel;
    Switch_ReadContacts: TSwitch;
    Layout7: TLayout;
    Label_WriteExternalStorage: TLabel;
    Switch_WriteExternalStorage: TSwitch;
    Layout8: TLayout;
    Label_ReadExternalStorage: TLabel;
    Switch_ReadExternalStorage: TSwitch;
    Layout9: TLayout;
    Label_RecordAudio: TLabel;
    Switch_RecordAudio: TSwitch;
    Layout10: TLayout;
    Layout11: TLayout;
    Layout12: TLayout;
    Button_Apply: TButton;
    Layout13: TLayout;
    Layout14: TLayout;
    Layout15: TLayout;
    Layout16: TLayout;
    Layout17: TLayout;
    Button_IsPermissionGranted: TButton;
    Layout18: TLayout;
    Layout19: TLayout;
    ImageControl1: TImageControl;
    Label1: TLabel;
    procedure Android_PermissionApplyResult(const Sender: TObject; GrantedResult: Boolean;
      NoGranteds: TArray<System.string>);
    procedure Button_ApplyClick(Sender: TObject);
    procedure Button_IsPermissionGrantedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation
uses
  {$IFDEF ANDROID}
    Androidapi.Helpers,
    Androidapi.JNI.JavaTypes,
    Androidapi.JNI.Os;
  {$ENDIF}

{$R *.fmx}



procedure TForm4.Android_PermissionApplyResult(const Sender: TObject; GrantedResult: Boolean;
  NoGranteds: TArray<System.string>);
var
  i : Integer;
begin
  if GrantedResult then
     Memo1.Lines.Add('All Granted OK')
  else
    begin
      Memo1.Lines.Add('Granted Fail');
      for i := 0 to High(NoGranteds) do
        Memo1.Lines.Add(NoGranteds[i]);
    end;
end;

procedure TForm4.Button_ApplyClick(Sender: TObject);
begin
  Memo1.Lines.Clear;
  if Switch_Camera.IsChecked then
     Android_Permission.Permissions := Android_Permission.Permissions + [CAMERA]
  else
     Android_Permission.Permissions := Android_Permission.Permissions - [CAMERA];

  if Switch_ReadContacts.IsChecked then
     Android_Permission.Permissions := Android_Permission.Permissions + [READ_CONTACTS]
  else
     Android_Permission.Permissions := Android_Permission.Permissions - [READ_CONTACTS];

  if Switch_ReadExternalStorage.IsChecked then
     Android_Permission.Permissions := Android_Permission.Permissions + [READ_EXTERNAL_STORAGE]
  else
     Android_Permission.Permissions := Android_Permission.Permissions - [READ_EXTERNAL_STORAGE];

  if Switch_WriteExternalStorage.IsChecked then
     Android_Permission.Permissions := Android_Permission.Permissions + [WRITE_EXTERNAL_STORAGE]
  else
     Android_Permission.Permissions := Android_Permission.Permissions - [WRITE_EXTERNAL_STORAGE];

  if Switch_RecordAudio.IsChecked then
     Android_Permission.Permissions := Android_Permission.Permissions + [RECORD_AUDIO]
  else
     Android_Permission.Permissions := Android_Permission.Permissions - [RECORD_AUDIO];

  Android_Permission.Apply;


//  var
//    Android_Permission : TAndroid_Permission;
//  try
//    Android_Permission := TAndroid_Permission.Create(nil);
//    Android_Permission.Permissions := [CAMERA];
//    //....
//    Android_Permission.Apply;
//  finally
//    Android_Permission.Free;
//  end;

end;

procedure TForm4.Button_IsPermissionGrantedClick(Sender: TObject);
var
  S : string;
begin
  Memo1.Lines.Clear;
  S := JStringToString(TJManifest_permission.JavaClass.CAMERA);
  Memo1.Lines.Add('Camera Granted: ' + Android_Permission.IsPermissionGranted(S).ToString(TUseBoolstrs.True));
end;

end.
